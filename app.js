require('dotenv').config();
const express = require('express');
const app = express();
const dbConfig = require('./Config/DB');
const port = process.env.PORT 
const bodyParser= require('body-parser');
const mongoose = require('mongoose');
const promotionRoutes = require('./Routers/PromotionsRoutes')
mongoose.connect(dbConfig.mongodb.url, {useNewUrlParser: true});
//mongoose.connect('mongodb://localhost/promotionsDB', {useNewUrlParser: true});
mongoose.set('useCreateIndex', true);

app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));
app.use(process.env.ROOT_PATH + '',promotionRoutes);

app.listen(port,function () {
	console.log("Server is running on port 1234")
});