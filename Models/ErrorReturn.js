function Reserr(code = null, message = null) { 
	this.code = code;
	this.message = message;
}
//Function to return a error 
Reserr.prototype.errorRepsonse = function (res) {
	res.status(this.code).send(this.message);
}

module.exports = Reserr;