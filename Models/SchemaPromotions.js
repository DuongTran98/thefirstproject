var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var JsonValidator = require('jsonschema').Validator;

let promotionsPackages = new Schema({
    packageId:String,
    url:String,
    text:String,
    order:String,
    createdAt:String,
    createdBy:String,
    active:{
        type:Boolean,
        default:true
    }
},{timestamps:true});

var promotions = new Schema({
    propertyId:String,
    accountId :String,
    title:String,
    subTitle:String,
    shortDetail:String,
    content:String,          //allow html
    special:{
        type:Boolean,
        default:true
    },
    removed:{
        type:Boolean,
        default:true
    },
    imageUrl:String,
    imageFileId:String,
    startAt:String,
    endAt:String,
    createdAt:String,
    createdBy:String,
    modifiedAt:String,
    modifiedBy:String,
    packages:[promotionsPackages]
},{timestamps:true,collection: "SchemaPromotions"});

promotions.set('toJSON', {
    virtuals: true,
    transform: function (doc, ret) { 
        delete ret._id
        delete ret.__v 
        delete ret.removed
    }
});

promotionsPackages.set('toJSON', {
    virtuals: true,
    transform: function (doc, ret) { 
        delete ret._id  
        delete ret.__v
    }
});

/**
* Check if the response is success
* @return {Boolean}      [true if success]
*/
// promotions.methods.toSimplePublicJson = function () {
//     return {
//         id: this.id,
//         propertyId: this.propertyId
//     }
// }

var promotionsValidator = {
    validate: function (data, isCreating) {
        var validator = new JsonValidator();
        
        JsonValidator.prototype.customFormats.promotionsContentType = function(input) {
            let whitelist = ['html'];
            return whitelist.indexOf(input) > -1;
        };
        
        var promotionsPackagesValidator = {
            id: "/promotionsPackages",
            type: "object",
            properties: {
                packageId: {"type": "string"},
                url: {"type": "string"},
                text: {"type": "string"},
                order: {"type": "int"},
                active:{"type":"boolean"}

            },
            additionalProperties: false,
            required: [ 
                "packageId",
                "url",
                "text",
            ]
        }
        if(!isCreating) {
            promotionsPackagesValidator.required = [];
        }
        
        var promotionsValidator = {
            id: "/promotionsData",
            type:"object",
            title:"Promotions",
            properties:{
                propertyId:{"type":"string"},
                title:{"type":"string"},
                subTitle:{"type":"string"},
                shortDetail:{"type":"string"},
                content:{"type": "string"},
                special:{"type":"boolean"},
                imageUrl:{"type":"string"},
                imageFileId:{"type":"string"},
                removed:{"type":"boolean"},
                packages:{
                    type:"array",
                    items:{"$ref":"/promotionsPackages"},
                    default:[]
                }
            },
            additionalProperties:false,
            required:[
                "propertyId", 
                "title",
                "subTitle",
                "shortDetail",
                "content",
                "special",
                "imageUrl",
                "imageFileId"
            ]
            
        }
        if(!isCreating) {
            promotionsValidator.required = [];
        }

        validator.addSchema(promotionsPackagesValidator, '/promotionsPackages');
        
        let validationResult = validator.validate(data, promotionsValidator);
        
        if(validationResult.errors && validationResult.errors.length > 0) {
            let formatErrors = {};
            for(let i = 0; i < validationResult.errors.length; i++) {
                let iError = validationResult.errors[i];
                if(iError) {
                    formatErrors[iError.property] = iError.message;
                }
            }
            
            return formatErrors;
        }
        
        return null;
    },
    
    validatePackages : function (data,isCreating) {
        var validator = new JsonValidator();
        
        // JsonValidator.prototype.customFormats.promotionsContentType = function(input) {
        //     let whitelist = ['html'];
        //     return whitelist.indexOf(input) > -1;
        // };
        var promotionsPackagesValidator = {
            id: "/promotionsPackages",
            type: "object",
            properties: {
                packageId: {"type": "string"},
                url: {"type": "string"},
                text: {"type": "string"},
                order: {"type": "string"},
                active:{"type":"boolean"}
            },
            additionalProperties: false,
            required: [ 
                "packageId",
                "url",
                "text",
            ]
        }
        if(!isCreating) {
            promotionsPackagesValidator.required = [];
        }
        let validationResult = validator.validate(data, promotionsPackagesValidator);
        
        if(validationResult.errors && validationResult.errors.length > 0) {
            let formatErrors = {};
            for(let i = 0; i < validationResult.errors.length; i++) {
                let iError = validationResult.errors[i];
                if(iError) {
                    formatErrors[iError.property] = iError.message;
                }
            }
            
            return formatErrors;
        }
        return null;
    }
}
module.exports={
    schema: mongoose.model('promotions',promotions),
    packagesSchema: mongoose.model('promotions_packages',promotionsPackages),
    validator: promotionsValidator
}
