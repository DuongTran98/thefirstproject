const jwt = require('jsonwebtoken');
const base64url = require('base64url');
const key = require('./Key');
//Check permisson of the token after decoded
function checkPermission(value, permissions) {
    if (permissions.includes(value)) return true;
    return false;
}
//Decode the token 
module.exports = function (options) {
    return function (req, res, next) {
        
        const token = req.headers.authorization;
        
        if (!token) {
            return res.status(400).send({
                message: "You must get access token first"
            })
        }

        try {
           const authorization = token.split(" ")[1];
            key().then(encode =>{
                let key = base64url.decode(encode); 
                let decode = jwt.verify(authorization, key, {
                    algorithms: ['RS256']
                });
                if (!decode) {
                    return res.status(404).send({
                        message: "Session not found"
                    })
                } else {
                    let key = decode.permissions;
                    
                    if (checkPermission(options, key)) {
                        req.decode = decode;                
                        return next();
                    } else {
                        return res.status(403).send({
                            message: "You dont have permission for action"
                        })
                    }
                }
            }).catch(err => {
                console.log(err)
                return res.status(400).send({
                    Error: err
                });
            })
        } catch (err) {
            return res.status(500).send({
                message:'Auth failed'
            });
        }
        
    }
    
}
