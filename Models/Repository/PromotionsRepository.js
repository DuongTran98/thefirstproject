const Promotions = require('../SchemaPromotions');
const PromotionsModel = require('../SchemaPromotions').schema;
const PromotionPackagesModel = require('../SchemaPromotions').packagesSchema;
const PromotionsValidator = require('../SchemaPromotions').validator;
const Promise = require('promise');
const ErrorReturn = require('../ErrorReturn');


module.exports = {
    findAllPromotions:function (query,decode) {
        return new Promise(function (resolve,reject) {

            try {
                if(!decode){
                    reject(new ErrorReturn(400, "Invalid decode data"))
                    return;
                }
                var model = PromotionsModel;
                var condition = {
                    removed: true
                };
                if (query.propertyId) {
                    condition['propertyId'] = query.propertyId;
                    return;
                }
                if (query.typeId) {
                    condition['typeId'] = query.typeId;
                    return;
                }
                var title = query.title;
                if (query.title) {
                    condition['title'] = new RegExp(title, "i");
                }
                //Thêm ".populate('FieldName')" vào sẽ hạn chế bị hiển thị dữ liệu trong mảng ra quá nhiều gây chậm api
                var model = model.find(condition).populate('packages');
                if (query) {
                    if (query.limit) {
                        model.limit(parseInt(query.limit));
                    }
                    if(query.page){
                        model.skip(parseInt(query.page)-1*parseInt(query.limit))
                    }
                    
                    if (query.order) {
                        var orderData = {};
                        orderData[query.order] = (query.dir && query.dir == 'desc') ? -1 : 1;
                        model.sort(orderData);
                    }
                }
                model.exec(function (err, docs) {
                    if (err) {
                        reject(new ErrorReturn(400, "There is an error occurred when trying to get promotions data"));
                        return;
                    }
                    
                    resolve(docs);
                    return;
                })   
            } catch (err) {
                reject(new ErrorReturn(500,err.message||"Failed"))   
                return;
            }
        })
        
    },
    findOnePromotions:function (id,decode) {
        return new Promise(function (resolve,reject) {
            try {
                if(!decode){
                    reject(new ErrorReturn(400, "Invalid decode data"))
                    return;
                }
               
                if(!decode){
                    reject(new ErrorReturn(400, "Invalid decode data"))
                    return;
                }
                PromotionsModel.findById({_id:id}).exec(function (err,doc) {
                    if(err){
                        reject(new ErrorReturn(400),err.message)
                        return;
                    }
                    resolve(doc)
                })
            } catch (err) {
                reject(new ErrorReturn(500,err.message||"Something was wrong here")) 
                return; 
            }
        })
    },
    createOnePromotions:function (data,decode) {
        return new Promise(function (resolve,reject) {
            try {
                if(!decode ){
                    reject(new ErrorReturn(400, "Invalid decode data"))
                    return;
                }
                if(!data){
                    reject(new ErrorReturn(404, "Cant create data"))
                    return;
                }
               
                var validateResult = PromotionsValidator.validate(data, true);   
                data.accountId = decode.acc;
                if(decode.sub) {
                    data.modifyBy = decode.sub;
                    data.createdBy = decode.sub;
                }
        
                if (validateResult){
                   reject(new ErrorReturn(400,"Failed"));
                   return;
                }
                
                const promotionsCreate = new PromotionsModel(data);
                promotionsCreate.save(function (err,promotionsCreate) {
                    if(err){
                        return reject(new ErrorReturn(500, "There is an error occured when trying to create new promotions"));
                    }
                    resolve(promotionsCreate);
                })
              
            } catch (err) {
                reject(new ErrorReturn(500,err.message))
                return;
            }
        })
    },
    putOnePromotions:function (id,data,decode) {
        return new Promise(function (resolve,reject) {
            try{
                if(!decode){
                    reject(new ErrorReturn(404, "Cant find any token! Pls input token to success"))
                    return;
                }
                if(!data){
                    reject(new ErrorReturn(404, "Cant find any data"))
                    return;
                }
                var validateResult = PromotionsValidator.validate(data, false);
                if (decode.sub){
                    data.modifyBy = decode.sub;
                }
             
                if (validateResult){
                    reject(new ErrorReturn(400, "Invalid Data"));
                    return;
                }
                PromotionsModel.findByIdAndUpdate({_id:id},data,(err,doc)=>{
                    if (err){
                        return reject(new ErrorReturn(400, err.message||"Cant update,pls check the source code again!"));
                    }
                    return resolve(doc)
                });
            }catch (err) {
                reject(new ErrorReturn(500, err.message));
                return;
            }
        })
    },
    deletePromotions: function (id,decode) {
        return new Promise(function (resolve,reject) {
            try {
                if(!decode){
                    reject(new ErrorReturn(400, "Invalid decode data"))
                    return;
                }
                PromotionsModel.findOneAndUpdate({_id:id},{removed:false},(err,doc)=>{
                    if(err){
                        return reject(new ErrorReturn(400, err.message||"Cant delete this field,pls check the source code again!"));
                    }
                    return resolve(doc)
                })
            } catch (err) {
                reject(new ErrorReturn(500, err.message));
                return;
            }
        })
    },

    createPackages: function (id,data,decode) {
        return new Promise(function (resolve,reject) {
            try {
                if(!data){
                    reject(new ErrorReturn(404,"Data Empty"))
                    return;
                }
                if(!decode){
                    reject(new ErrorReturn(400, "Invalid decode data"))
                    return;
                }
                var validateResult = PromotionsValidator.validatePackages(data, true);
                if (validateResult){
                    reject(new ErrorReturn(400, "Invalid Data"));
                    return;
                }
                PromotionsModel.findByIdAndUpdate({_id:id},{$push:{packages:data}},{upsert: true, save: true},(err,doc)=>{
                    if(err){
                        return reject(new ErrorReturn(400, err.message||"Cant create this field,pls check the source code again!"));
                    }
                    return resolve(doc);
                })
            } catch (err) {
                reject(new ErrorReturn(500, err.message));
                return;
            }
        })
    },
    findAllPackages:function (id,decode) {
        return new Promise(function (resolve,reject) {
            try {
                if(!decode){
                    reject(new ErrorReturn(400, "Invalid decode data"))
                    return;
                }
                var condition = {
                    _id:id
                }
                PromotionsModel.findById(condition,(err,doc)=>{
                    if(err){
                        return reject(new ErrorReturn(400, err.message||"Cant find this field,pls check the source code again!"));
                    }
                   var data = doc.packages;
                   var result = data.filter(({active}) => active === true)
                   return resolve(result)
                })

            } catch (err) {
                reject(new ErrorReturn(500, err.message));
                return;
            }
        })
    },

    findOnePackages:function (id,idPackages,decode) {
        return new Promise(function (resolve,reject) {
            try {
                if(!decode){
                    reject(new ErrorReturn(400, "Invalid decode data"))
                    return;
                }
                PromotionsModel.findById({_id:id},(err,doc) => {
                    if(err){
                        return reject(new ErrorReturn(400, err.message||"Cant find this field,pls check the source code again!"));
                    }
                    var data = doc.packages;
                    var result = data.find(({id}) => id === idPackages);
                    resolve(Array(result));
                })
            } catch (err) {
                reject(new ErrorReturn(500, err.message));
                return;
            }
        })
    },
    updatePackages:function (id,idPackages,data,decode) {
        return new Promise(function (resolve,reject) {
            try { 
                if(!decode){
                    reject(new ErrorReturn(400, "Invalid decode data"))
                    return;
                }
                if(!data){
                    reject(new ErrorReturn(400, "Invalid decode data"))
                    return;
                }
                var validateResult = PromotionsValidator.validatePackages(data, true);
                if (validateResult){
                    reject(new ErrorReturn(400, "Invalid Data"));
                    return;
                }
                var condition = {
                    _id:id,"packages":{$elemMatch:{"_id": idPackages}}
                }
                var dataUpdate = {
                    "packages.$[element].packageId": data.packageId,
                    "packages.$[element].url": data.url,
                    "packages.$[element].text": data.text,
                    "packages.$[element].order": data.order,
                };             
                var excluded = {
                    $set: dataUpdate
                }
                PromotionsModel.updateOne(condition,excluded,{arrayFilters: [{ "element._id": idPackages }]},function (err,doc) {
                    if (err) {
                        return reject(new ErrorReturn(400, err.message||"Cant find this field,pls check the source code again!"));  
                    }
                    return resolve(doc)
                })
            } catch (err) {
                reject(new ErrorReturn(500, err.message));
                return;
                
            }
        })
    },
    deletePackages:function (id,idPackages,decode) {
        return new Promise(function (resolve,reject) {
            try { 
                if(!decode){
                    reject(new ErrorReturn(400, "Invalid decode data"))
                    return;
                }
                var condition = {
                    _id:id,"packages":{$elemMatch:{"_id": idPackages}}
                }
                var dataDelete = {
                    "packages.$[element].active":false,
                   
                };             
                var excluded = {
                    $set: dataDelete
                }
                PromotionsModel.update(condition,excluded,{arrayFilters: [{ "element._id": idPackages }]},function (err,doc) {
                    if (err) {
                        return reject(new ErrorReturn(400, err.message||"Cant find this field,pls check the source code again!"));  
                    }
                    return resolve(doc)
                })
            } catch (err) {
                reject(new ErrorReturn(500, err.message));
                return;             
            }
        })
    }
}
