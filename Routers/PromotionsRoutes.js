const express = require('express');
const promotionsController = require('../Controllers/Promotions');
const router = express.Router();
const checkAuth = require('../Middleware/Check-auth');

router.get('/',checkAuth('pro:r'), promotionsController.getAllPromotions);
router.get('/:id',checkAuth('pro:r'), promotionsController.getOnePromotions);
router.post('/',checkAuth('pro:m'),promotionsController.postOnePromotions);
router.put('/:id',checkAuth('pro:m'),promotionsController.updateOnePromotions);
router.delete('/:id',checkAuth('pro:m'),promotionsController.deleteOnePromotions);

router.post('/:id/packages',checkAuth('pro:m'),promotionsController.postOnePackages);
router.get('/:id/packages',checkAuth('pro:r'),promotionsController.getAllPackages);
router.get('/:id/packages/:idPackages',checkAuth('pro:m'),promotionsController.getOnePackages);
router.put('/:id/packages/:idPackages',checkAuth('pro:m'),promotionsController.updateOnePackages);
router.delete('/:id/packages/:idPackages',checkAuth('pro:m'),promotionsController.deleteOnePackages);
module.exports = router;