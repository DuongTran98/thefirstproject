FROM node:10.10.0
WORKDIR /TheFirstProject
# COPY package.json /TheFirstProject
COPY . /TheFirstProject 
RUN npm install

CMD [ "node", "." ]