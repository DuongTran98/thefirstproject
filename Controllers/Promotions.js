const PromotionsRepository = require('../Models/Repository/PromotionsRepository');
const ErrorReturn = require('../Models/ErrorReturn');

module.exports = {
    //PR001 
    getAllPromotions : function (req,res) {
        var query = req.query;
        var decode = req.decode;  

        try {
            PromotionsRepository.findAllPromotions(query,decode).then(docs => {
                res.status(200).json(docs);
                
            }).catch(err => {
                (new ErrorReturn(400,err.message||"Some mistake")).errorRepsonse(res);
                return;
            })
        } catch (err) {
            (new ErrorReturn(500, err.message)).errorRepsonse(res);
            return;
        }
    },
    //PR003
    getOnePromotions : function (req,res) {
        var id = req.params.id;
        var decode = req.decode;  

        try {
            PromotionsRepository.findOnePromotions(id,decode).then(doc => {
               
                res.status(200).json(doc);
            }).catch(err => {
                (new ErrorReturn(400,err.message||"Something was wrong here")).errorRepsonse(res);
                return;
            })
        } catch (err) {
            (new ErrorReturn(500, err.message)).errorRepsonse(res);
            return;
        }
    },
    //PR004
    postOnePromotions :function (req,res) {
        var data = req.body;
        var decode = req.decode;

        try {
            PromotionsRepository.createOnePromotions(data,decode).then(doc => {
                res.status(201).json(doc);
            }).catch(err => {
                (new ErrorReturn(400,err.message)).errorRepsonse(res);
                return;
            })
        } catch (err) {
            (new ErrorReturn(500, err.message)).errorRepsonse(res);
            return;
        }
    },
    //PR005
    updateOnePromotions : function (req,res) {
        var id = req.params.id; 
        var data = req.body;
        let decode = req.decode;   
       
        try {
            PromotionsRepository.putOnePromotions(id,data,decode).then(doc => {
                res.status(200).json(doc);
            }).catch(err => {
                (new ErrorReturn(400,err.message||"Something was wrong here")).errorRepsonse(res);
                return;
            })
        } catch (err) {
            (new ErrorReturn(500, err.message)).errorRepsonse(res);
            return;
        }
    },
    //PR006
    deleteOnePromotions : function (req,res) {
        var id = req.params.id;
        var decode = req.decode;

        try {
            PromotionsRepository.deletePromotions(id,decode).then(doc => {
                res.status(204).json("Delete Success");
            }).catch(err => {
                (new ErrorReturn(400,err.message||"Something was wrong here")).errorRepsonse(res);
                return;
            })
        } catch (err) {
            (new ErrorReturn(500, err.message)).errorRepsonse(res);
            return;
        }
    },
    //PR008
    postOnePackages : function (req,res) {
        var id = req.params.id;
        var data = {
            packageId : req.body.packageId,
            url : req.body.url,
            text : req.body.text,
            order : req.body.order
        }
        var decode = req.decode;

        try {
            PromotionsRepository.createPackages(id,data,decode).then(doc => {
                res.status(201).json(doc);
            }).catch(err => {
                (new ErrorReturn(400,err.message||"Something was wrong here")).errorRepsonse(res);
                return;
            })
        } catch (err) {
            (new ErrorReturn(500, err.message)).errorRepsonse(res);
            return;
        }
    },
    //PR007
    getAllPackages : function (req,res) {
        var id = req.params.id;
        var decode = req.decode;

        try {
            PromotionsRepository.findAllPackages(id,decode).then(doc => {
                res.status(200).json(doc);
            }).catch(err => {
                (new ErrorReturn(400,err.message||"Something was wrong here")).errorRepsonse(res);
                return;
            })
        } catch (err) {
            (new ErrorReturn(500, err.message)).errorRepsonse(res);
            return;
        }
    },
    //PR009
    getOnePackages : function (req,res) {
        var id = req.params.id;
        var idPackages = req.params.idPackages;
        var decode = req.decode;

        try {
            PromotionsRepository.findOnePackages(id,idPackages,decode).then(doc => {
                res.status(200).json(doc);
            }).catch(err => {
                (new ErrorReturn(400,err.message||"Something was wrong here")).errorRepsonse(res);
                return;
            })
        } catch (err) {
            (new ErrorReturn(500, err.message)).errorRepsonse(res);
            return;
        }
    },
    //PR010
    updateOnePackages :function (req,res) {
        var id = req.params.id;
        var idPackages = req.params.idPackages;
        var decode = req.decode;
        var data = req.body;

        try {
            PromotionsRepository.updatePackages(id,idPackages,data,decode).then(doc => {
                res.status(200).json(doc);
            }).catch(err => {
                (new ErrorReturn(400,err.message||"Something was wrong here")).errorRepsonse(res);
                return;
            })
        } catch (err) {
            (new ErrorReturn(500, err.message)).errorRepsonse(res);
            return;
        }
    },
    //PR011
    deleteOnePackages :function (req,res) {
        var id = req.params.id;
        var idPackages = req.params.idPackages;
        var decode = req.decode;

        try {
            PromotionsRepository.deletePackages(id,idPackages,decode).then(doc => {
                res.status(201).json("Delete Success");
            }).catch(err => {
                (new ErrorReturn(400,err.message||"Something was wrong here")).errorRepsonse(res);
                return;
            })
        } catch (err) {
            (new ErrorReturn(500, err.message)).errorRepsonse(res);
            return;
        }
    },

}
