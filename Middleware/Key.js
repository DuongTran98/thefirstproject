const request = require("request");
const url = process.env.JWT;
//Get token key 
module.exports = () => {
    return new Promise((resolve,reject)=>{
        request({
            url: url,
            json: true
        }, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                let jsonKey = body.keys[0].n; 
                resolve(jsonKey);
            }
            reject(error)
        })
    })
}